package com.dws.challenge.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class FundTransferRequest {
    private String fromAccount;
    private String toAccount;
    @NotNull
    @Min(value = 0, message = "Initial balance must be positive.")
    private BigDecimal amount;
    private String authID;
}
