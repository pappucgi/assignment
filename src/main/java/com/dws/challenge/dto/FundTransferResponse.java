package com.dws.challenge.dto;

import lombok.Data;

@Data
public class FundTransferResponse {
    private String message;
    private String transactionId;
}
